import React from 'react';
import { StyleSheet, Text,Button, View,SafeAreaView,AsyncStorage } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

//Components
import MyComponent from './components/MyComponent';
import ShowPosition from './components/ShowPosition';
import ListComponent from './components/ListComponent';
import Login from './components/Login';
import ToDoComponent from './components/ToDoComponent';
import AuthLoaderComponent from './components/AuthLoaderComponent';
import DrawerComponent from './components/DrawerComponent';
import CameraComponent from './components/CameraComponent';
/*STACK DI NAVIGAZIONE*/
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator,DrawerActions } from 'react-navigation-drawer';


class App extends React.Component{
  static navigationOptions = {
    title: 'App'
  };
  render(){
    return ( 
      <View style={{flex:1,justifyContent:'center'}}>
        
      </View>
      
    );
  }
}
const authNavigator = createStackNavigator({
  Login: {
    screen:Login,
  }
},{
  headerMode:'none'
})

const tabNavigator = createBottomTabNavigator({
  Home: {screen:ListComponent},
  Location: {screen:ShowPosition} ,
  Camera: {screen:CameraComponent} ,
 /* Drawer:{
    screen:DrawerNavigator,
  }*/
},{
   initialRouteName:'Home',
   defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({tintColor}) => {
        const { routeName } = navigation.state;
        let name;

        switch(routeName){
          case 'home':
            name= "home";
            break;
          case 'Location':
            name='navigation';
          break;
          case 'Camera':
            name='camera';
          break;
          default:
            name= "home";
        }
        return <Icon name={name} size={20} type="material-community" color={tintColor} />
      }
   }),
   tabBarOptions:{
     activeTintColor: 'green',
     inactiveTintColor:'blue',
     labelStyle:{
       fontSize:15
     },
     tabStyle:{
       justifyContent:'center'
     }
   }
});
const DrawerNavigator= createDrawerNavigator({
  Home:{
    
    screen:ListComponent,
    navigationOptions: {
      drawerLabel: ()=>'Liste',
    }
  },
  Location:{screen:ShowPosition},
  Tabs:{
    screen:tabNavigator,
    navigationOptions: {
      drawerLabel: ()=>null,
    }
  },
  Camera: {screen:CameraComponent} ,
  Logout:{
    screen: ({navigation})=> {
      AsyncStorage.removeItem('token');
      navigation.navigate('AuthLoader');
      return null;
    },
  }
},{
  drawerPosition:'right',
  initialRouteName:'Tabs',
  contentComponent:DrawerComponent
});
const mainNavigator= createStackNavigator({
  Auth: {
    screen: authNavigator,
    navigationOptions: {
      header:null
    }
  },
  ToDo:{
    screen:ToDoComponent,
  },
  Tabs:tabNavigator, 
  Drawer:DrawerNavigator
},{
  defaultNavigationOptions: ({navigation,screenProps,navigationOptions}) => ({
    headerRight:(

      <Icon name="menu" size={30} onPress={ () => navigation.dispatch(DrawerActions.toggleDrawer())}> </Icon>
      
    ),
    headerLeft:null,
  })
});
export default createAppContainer(createSwitchNavigator({
  Auth:authNavigator,
  AuthLoader: AuthLoaderComponent,
  Main: mainNavigator,
  //Tab:tabNavigator
},{
  initialRouteName:'AuthLoader'
}));
const styles = StyleSheet.create({ 
  container: {
    flex: 1,
    justifyContent: 'center',
  },
});
 