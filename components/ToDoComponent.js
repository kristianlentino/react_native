import React from 'react';
import { StyleSheet, Text,SafeAreaView ,View,ActivityIndicator,TextInput,TouchableOpacity,Button,FlatList } from 'react-native';
import ToDo from '../models/ToDo';

export default class ToDoComponent extends React.Component {

    constructor(props){ 
        super(props);
        this.state={
            isLoading:true,
            todo: null
        }
    }
    static navigationOptions =({navigation,screenProps,navigationOptions}) => ({
        title: 'Todo ' +  navigation.getParam('id'),
        headerRight:null
    });
    componentDidMount(){
        let idTodo= this.props.navigation.getParam('id');
        fetch(`https://jsonplaceholder.typicode.com/todos/${idTodo}`).then(response => response.json())
        .then(todo => {
           this.setState({
               todo,
               isLoading:false
           });
        
        });
    
    }

    render(){
        const view= this.state.isLoading 
            ?  <ActivityIndicator animating={this.state.isLoading} size="large" color="#0000ff"></ActivityIndicator>
            : <Text style={styles.item}>{this.state.todo.id} -  {this.state.todo.title} </Text>
        return(
            <SafeAreaView>
                {view}
            </SafeAreaView>
        );
    }

}
const styles = StyleSheet.create({
    item:{
        marginLeft:10,
        marginTop:10,
    }
})
