import React from 'react';
import { StyleSheet, Text, View,Image,ActivityIndicator } from 'react-native';
import {PropTypes} from 'prop-types';
import MapView from 'react-native-maps'; 
//Functions
import {checkGeolocationPermissionAsync, askGeolocationPermissionAsync} from '../functions/permissions';
export default class ShowPosition extends React.Component {

    constructor(props){ 
        super(props);
        this.state= {
            isLoading:true,
            position:{

            }
        };
    } 
    componentDidMount = async () => {
        let ask = await askGeolocationPermissionAsync();
        if (!ask) {
        } else {
            navigator.geolocation.watchPosition(
                position =>this.setState({
                    isLoading:false,
                    position  //javascript prende di default la variabile che si chiama position nello scope corrente 
                }),
                err => console.error(err), {
                    enableHighAccuracy: true
                }
            );
        }
    };
    
    //componentDidMount(){
        /*checkGeolocationPermissionAsync().then(hasLocationAccess=> {
            //alert(hasLocationAccess? 'si' : 'No');  
            if(hasLocationAccess){
                console.log(navigator.geolocation.watchPosition(
                    pos =>console.log(pos)
                ));
            } 
        });*/
     
   // };



    render(){

        const showThis = (this.state.isLoading)
        ?  <ActivityIndicator
                animating={this.state.isLoading}
                color="#0000FF"
                size="large"
            ></ActivityIndicator>
        : <Text>{this.state.position.coords.latitude + ' '+ this.state.position.coords.longitude}</Text>;
        
        /*if(this.state.isLoading){
            const loader= <View style={styles.container}>
            <Image source={require('../assets/images/loader.gif')} style={{width:50,height:50}}></Image>
            <Image source={require('../assets/images/loader2.gif')} style={{width:50,height:50}}></Image>
            </View>;
                
        }*/
        return (
            <View style={styles.container}>

                {showThis}
               
            </View>

        )
    }

}
ShowPosition.defaultProps= { //setta di default un testo se non gli viene passata la prop text
    text: 'Ciao a tutti'
}
ShowPosition.propTypes= {
    text: PropTypes.string
}
const styles = StyleSheet.create({ 
    container: { 
      flex:1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    padding: {
        padding:10
    },
    map: {
        flex:1,
        alignItems: 'center',
        ...StyleSheet.absoluteFillObject,
    },
});