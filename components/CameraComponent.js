import React from 'react';
import {View,AsyncStorage,SafeAreaView,ScrollView,StyleSheet} from 'react-native';
import {Camera} from 'expo-camera';
import {askCameraPermissionAsync} from '../functions/permissions';
import { NavigationEvents } from 'react-navigation';

export default class CameraComponent extends React.Component {

    constructor(props){
        super(props);
        this._camera=null;
        this.state={
            camera: Camera.Constants.Type.back,
            hasPermissions:null


        };
    }
    componentDidMount = async () => {
        let ask = await askCameraPermissionAsync(); 
        if (!ask) {
        } else {
            alert('grazie');
        }
    };
    componentWillUnmount= () => {
        this._camera=null;
    }
    render(){
        return(
            
            <Camera
                ratio='16:9'
                style={styles.camera}
                ref= { ref=> this._camera=ref}
                type={this.state.camera}
            >

            </Camera>
            
            
        );

    }

}
const styles= StyleSheet.create({
    camera:{
        flex:1,
        width:'100%'
    }
})