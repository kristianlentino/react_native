import React from 'react';
import { StyleSheet, Text, View,ImageBackground,TextInput,TouchableOpacity,Button,AsyncStorage } from 'react-native';
import {PropTypes} from 'prop-types';
//import AsyncStorage from '@react-native-community/async-storage';


export default class Login extends React.Component {
    
    constructor(props){ 
        super(props);
        
        this.state={
            username: '',
            password:''
        }
    }

    static navigationOptions = {
        header: null
    };
    
    onChangeText= (text) => {
        this.setState({
            username: text
        });
    }
    
    onChangePassword= (text) => {
        this.setState({
            password: text
        });
    }
    handleLogin= ()=> {
        
        fetch('https://reqres.in/api/login',{
            method: 'POST',
            headers: {
                Accept: '*/*',
                'Content-Type': 'application/json; charset=utf-8',
            },
            body: JSON.stringify({
                email: this.state.username,
                password:  this.state.password,
            }),
        }).then((response) => response.json())
        .then((responseJson) => {
            if(responseJson.token){
                alert('Login successfull');
                AsyncStorage.setItem('token',responseJson.token);
                this.props.navigation.navigate('Drawer');

            } else{
                alert('Invalid user or password');
            }
          });
    }
    render(){
        return (
            <ImageBackground  source={require('../assets/images/sfondoLogin.jpg')} style={styles.imgBackground}>
                <View  style={styles.textContainer}>
                    <Text style={{ alignSelf:'center',color:'white'}}>Username </Text>
                    <TouchableOpacity style={{alignSelf:'center',width: '80%'}} >
                        <TextInput
                            clearButtonMode={'always'}
                            autoCompleteType={"username"}
                            style={styles.textInput}
                            placeholder='Please enter your username'
                            underlineColorAndroid='transparent'
                            onChangeText={text => this.onChangeText(text)}
                            value={this.state.username}
                        />
                    </TouchableOpacity>
                    
                    <Text style={{ alignSelf:'center',color:'white'}}>Password </Text>
                    <TouchableOpacity style={{alignSelf:'center',width: '80%'}} >
                        <TextInput
                            autoCompleteType={"password"}
                            secureTextEntry={true}
                            style={styles.textInput}
                            placeholder='Please enter your password'
                            underlineColorAndroid='transparent'
                            onChangeText={text => this.onChangePassword(text)}
                            value={this.state.password}
                        />
                    </TouchableOpacity>
                    <Button
                        title="LOGIN"
                        style={{width:'20%'}}
                        onPress={() => this.handleLogin()}
                    />
                </View>
                     
                     
            </ImageBackground>
        )

    };
};

const styles = StyleSheet.create({ 
    container: {
      flex: 1,
      backgroundColor: '#D50000',

    },
    imgBackground: {
        flex:1,
        width: '100%', 
        height:'100%',
        justifyContent:'center',
    },
   /* textInput:{
        alignSelf:'center',
        height: 40,
        width: '80%',
        borderBottomColor: 'gray',
        borderBottomWidth: 1,
        marginLeft:'3%'
    },*/
    textInput:{
        alignSelf:'stretch',    
        padding: 10,
        marginBottom: 20,
        height: 40,
        borderBottomColor: 'gray',
        borderBottomWidth: 1,
        marginLeft:'3%'
    },
    textContainer:{
        flex:1,
        padding:25,
        justifyContent:'center',
        
    }
  });
