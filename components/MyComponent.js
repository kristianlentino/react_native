import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {PropTypes} from 'prop-types';


export default function MyComponent(props) {
    
    return (
        <View>
            <Text>{props.text}</Text>
        </View>
        
    );
    
}
MyComponent.defaultProps= {
    text: 'My Component'
}
MyComponent.propTypes= {
    text: PropTypes.string
}
