import React from 'react';
import { StyleSheet, Text,SafeAreaView ,View,ActivityIndicator,TextInput,TouchableOpacity,Button,FlatList } from 'react-native';
import ToDo from '../models/ToDo';

export default class ListComponent extends React.Component {

    constructor(props){ 
        super(props);
        this.state={
            isLoading:true,
            todos: []
        }
    }
    static navigationOptions = {
        title: 'Lista todo'
    };
    componentDidMount(){
        fetch('https://jsonplaceholder.typicode.com/todos').then(response => response.json())
        .then(json => {
            const todos=[];

            for (const todo of json){
                todos.push(new ToDo(todo.id,todo.title));
            }

            //console.log(todos);
            this.setState({
                todos,
                isLoading: false
            });
        })
    }
    render(){
        const view= this.state.isLoading 
            ?  <ActivityIndicator animating={this.state.isLoading} size="large" color="#0000ff"></ActivityIndicator>
            : <FlatList
                    data={this.state.todos}
                    renderItem={({ item }) => <TouchableOpacity onPress={ () => {
                        this.props.navigation.navigate('ToDo',{
                            id: item.id
                        });
                    }}><Text style={{height:35 }}> {item.text}</Text></TouchableOpacity> }
                    keyExtractor={item => item.id.toString()}
                    style={{width:'100%',padding:10}}
                />;
        return (
            <SafeAreaView style={{flex:1}}>
                {view}
            </SafeAreaView> 
        );
    }
}