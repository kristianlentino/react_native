import React from 'react';
import {View,AsyncStorage,SafeAreaView,ScrollView} from 'react-native';
import { DrawerNavigatorItems } from 'react-navigation-drawer';

export default class DrawerComponent extends React.Component {
    constructor(props){ 
        super(props);
    }
    render(){
        return(
            <ScrollView>
                <SafeAreaView
                    forceInset={{top:'always',horizontal:'never'}}
                >
                    <DrawerNavigatorItems {...this.props}></DrawerNavigatorItems>
                </SafeAreaView>
            </ScrollView>
            
        );
    }

}
