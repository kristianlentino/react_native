import React from 'react';
import {View,AsyncStorage} from 'react-native';

export default class AuthLoaderComponent extends React.Component {

    constructor(props){ 
        super(props);
    }

    async componentWillMount(){

        const token = await AsyncStorage.getItem('token');
        if(token){
            this.props.navigation.navigate('Drawer');

        } else {
            this.props.navigation.navigate('Auth');
        }
    }
    render(){
        return(
            <View></View>
        );
    }

}