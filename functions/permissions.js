import {askAsync,AUDIO_RECORDING,CAMERA,LOCATION,getAsync} from 'expo-permissions';


/*Controllo automatico i permessi*/
export async function checkGeolocationPermissionAsync(showAlert=false){
    
    const { status } = await getAsync(LOCATION);

    if(status!=='granted'){

        if(showAlert){

            alert('Please grant location permissions to the app');
            
        }

        return false;
        
    }
    return true;
}
/*Chiede in automatico i permessi di geolocalizzazione*/
export async function askGeolocationPermissionAsync(showAlert=false) {
    
    const { status } = await askAsync(LOCATION);

    if(status!=='granted'){

        if(showAlert){

            alert('Please grant location permissions to the app');
            
        }

        return false;
        
    }
    return true;
}

export async function askCameraPermissionAsync(showAlert=false) {
    
    const { status } = await askAsync(AUDIO_RECORDING,CAMERA);

    if(status!=='granted'){

        if(showAlert){

            alert('Please grant camera and audio permissions to the app');
            
        }

        return false;
        
    }
    console.log('ciao');
    return true;
}